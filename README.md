# WA-Partyinsel

### Bisher: 
Tilesammlung,
Insel-Map, Hexenhaus-Map

### Geplant: 
Vulkaninneres als Map
-> Tiles für Vulkaninneres, Bar und Ausstattung

Die Insel als Map 
-> Grobkonzept steht. :)
Verbesserungspotential: 
- Hindernisse, 
- jitsi, 
- Animationen, 
- objekte überhalb des FloorLevels, 
- Ausgang (optional), 
- Bäume und Deko
- Musik/Geräusche (bin da allerdings unentschlossen.)
- Innenräume (siehe unten).

### Optional: 
modifizierte Tiles und/oder Tilesets, damit visuell alles besser passt und um Daten zu sparen


### Bonus: 
Unterwasserwelt oder Wasserfallhöhle als Map

Hafenkneipe als Map

Hexennhaus als Map
